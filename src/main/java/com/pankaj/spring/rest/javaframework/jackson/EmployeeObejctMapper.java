package com.pankaj.spring.rest.javaframework.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pankaj.spring.rest.javaframework.jackson.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class EmployeeObejctMapper {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public Employee readJsonWithObjectMapper() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Employee employee = objectMapper.readValue(new File("target/classes/employee_US.json"), Employee.class);
        logger.info(employee.toString());
        return employee;
    }
}
