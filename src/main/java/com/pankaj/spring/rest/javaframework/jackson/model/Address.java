package com.pankaj.spring.rest.javaframework.jackson.model;

import lombok.Data;

@Data
public class Address {

    private String street;
    private String city;
    private String zipCode;

}
